from conans import ConanFile, tools
import shutil

class CppPeglibConan(ConanFile):
    name           = "cpp-peglib"
    version        = "0.1.10"
    license        = "MIT"
    url            = "https://bitbucket.org/toge/conan-cpp-peglib"
    description    = "A single file C++ header-only PEG (Parsing Expression Grammars) library"
    no_copy_source = True
    homepage       = "https://github.com/yhirose/cpp-peglib"
    settings       = "os"

    def source(self):
        tools.get("https://github.com/yhirose/cpp-peglib/archive/v{}.tar.gz".format(self.version))
        shutil.move("cpp-peglib-{}".format(self.version), "cpp-peglib")

    def package(self):
        self.copy("peglib.h", dst="include", src="cpp-peglib")

    def package_info(self):
        self.info.header_only()
        if self.settings.os == "Linux":
            self.cpp_info.libs = ["pthread"]
