#include <iostream>
#include <cstdlib>

#include "peglib.h"

int main(int argc, const char** argv)
{
    auto reduce = [](const peg::SemanticValues& sv) {
        auto result = peg::any_cast<long>(sv[0]);
        for (auto i = 1u; i < sv.size(); i += 2) {
            auto num = peg::any_cast<long>(sv[i + 1]);
            auto ope = peg::any_cast<char>(sv[i]);
            switch (ope) {
                case '+': result += num; break;
                case '-': result -= num; break;
                case '*': result *= num; break;
                case '/': result /= num; break;
            }
        }
        return result;
    };

    auto parser = peg::parser(R"(
        EXPRESSION       <-  _ TERM (TERM_OPERATOR TERM)*
        TERM             <-  FACTOR (FACTOR_OPERATOR FACTOR)*
        FACTOR           <-  NUMBER / '(' _ EXPRESSION ')' _
        TERM_OPERATOR    <-  < [-+] > _
        FACTOR_OPERATOR  <-  < [/*] > _
        NUMBER           <-  < [0-9]+ > _
        ~_               <-  [ \t\r\n]*
    )");

    parser["EXPRESSION"]      = reduce;
    parser["TERM"]            = reduce;
    parser["TERM_OPERATOR"]   = [](const peg::SemanticValues& sv) { return static_cast<char>(*sv.c_str()); };
    parser["FACTOR_OPERATOR"] = [](const peg::SemanticValues& sv) { return static_cast<char>(*sv.c_str()); };
    parser["NUMBER"]          = [](const peg::SemanticValues& sv) { return std::atol(sv.c_str()); };

    auto expr = "5+3*3";
    auto val  = long{};
    if (parser.parse(expr, val)) {
        std::cout << expr << " = " << val << std::endl;
        return 0;
    }

    std::cout << "syntax error..." << std::endl;

    return -1;
}
